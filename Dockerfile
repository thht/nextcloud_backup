FROM debian:latest

RUN mkdir /scripts
RUN mkdir -p /root/.config/rclone

ADD backup.sh /scripts/backup.sh
ADD rustic_conf.toml /scripts/rustic_conf.toml
ADD rclone.conf /root/.config/rclone/rclone.conf

RUN chmod a+x /scripts/*.sh

CMD /scripts/backup.sh