#!/usr/bin/env bash
set -e

apt update

apt-get install -y rclone curl fuse kmod

curl -L -o rustic.tar.gz https://github.com/rustic-rs/rustic/releases/download/v0.3.1/rustic-v0.3.1-x86_64-unknown-linux-gnu.tar.gz
tar xfz rustic.tar.gz -C /usr/local/bin/
rustic self-update

rclone config create nextcloud_backup ftp host stor101.owncube.com user octhhtde@bk101.owncube.com tls false explicit_tls true pass $RCLONEPASS

mkdir /mnt/nextcloud

rclone mount --read-only --daemon nextcloud_backup:// /mnt/nextcloud

N_RETRIES=10

while ! grep -q -s "nextcloud_backup:" /proc/mounts; do
        echo "Waiting for mount"
        sleep 10
        ((N_RETRIES--))

        if [ $N_RETRIES -eq 0 ]
        then
                echo "mount failed"
                exit 1
        fi
done

hostname thht-nextcould-backup

rustic backup --verbose -P /scripts/rustic_conf

rustic forget --verbose --prune --instant-delete -P /scripts/rustic_conf